package cz.jaradeak.feedme;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Random;

/**
 * FeedMe project
 * Author: Jaromir Deak
 * E-mail: jaromir.deak@gmail.com
 */

public class MainActivity extends AppCompatActivity {
    private ImageButton imageButtonKebab, imageButtonPizza, imageButtonBurger, imageButtonSurprise;

    enum Meal{KEBAB, PIZZA, BURGER, SURPRISE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageButtonKebab = (ImageButton) findViewById(R.id.imageButtonKebab);
        imageButtonPizza = (ImageButton) findViewById(R.id.imageButtonPizza);
        imageButtonBurger = (ImageButton) findViewById(R.id.imageButtonBurger);
        imageButtonSurprise = (ImageButton) findViewById(R.id.imageButtonSurprise);

        imageButtonKebab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Search for restaurants nearby
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=kebab");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        imageButtonPizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Search for restaurants nearby
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=pizza");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        imageButtonBurger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Search for restaurants nearby
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=burger");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        imageButtonSurprise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random rand = new Random();
                int meal = rand.nextInt() % 3;
                Uri gmmIntentUri = null;

                switch (meal){
                    case 0:
                        gmmIntentUri = Uri.parse("geo:0,0?q=kebab");
                        break;
                    case 1:
                        gmmIntentUri = Uri.parse("geo:0,0?q=pizza");
                        break;
                    case 2:
                        gmmIntentUri = Uri.parse("geo:0,0?q=burger");
                        break;
                    default:
                        gmmIntentUri = Uri.parse("geo:0,0?q=meal");
                }

                // Search for restaurants nearby
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
//
            }
        });
    }
}
